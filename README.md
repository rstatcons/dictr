# R package dictr

It extracts pronunciation data from English-language [Wiktionary](https://en.wiktionary.org/wiki/Wiktionary:Main_Page). The idea is to generate [Anki](https://apps.ankiweb.net/) flash cards with English words containing [IPA](https://en.wikipedia.org/wiki/International_Phonetic_Alphabet) phonetic notation and audio records. 

Currently for a given English word you get all available	phonetic transcriptions and audio files (saved in working directory).

## Limitations / TODOs

* No checks on absence of the page or elements.
* The result is just an R-list and saved files, but not an archive [suitable](https://apps.ankiweb.net/docs/manual.html#importing) for export in Anki.


## Install

`devtools::install_git("git@gitlab.com:rstatcons/dictr.git")`

# Usage

```r
library(dictr)
extract_and_save("parachute")
```
```
$term
[1] "parachute"

$ipa
# A tibble: 1 x 2
   type         ipa
  <chr>       <chr>
1  <NA> /ˈpæɹəʃuːt/

$files
# A tibble: 1 x 3
                 name       desc                                                                     url
                <chr>      <chr>                                                                   <chr>
1 En-us-parachute.ogg Audio (US) https://upload.wikimedia.org/wikipedia/commons/8/8e/En-us-parachute.ogg
```
