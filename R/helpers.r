
#' Extract audio urls from HTML page
#' @import magrittr
#'
extract_audio_urls <- function(term_html) {

  lis <- rvest::html_nodes(
    x = term_html,
    # https://stackoverflow.com/a/23862266/3190110
    xpath = "//ul[count(preceding-sibling::h2)=1 and
    preceding-sibling::h2/span/@id='English']/li[span/@class='IPA']")

  descs <- purrr::map_chr(lis, ~rvest::html_nodes(
    .,
    xpath = "span[not(@class = 'IPA') and @class = 'ib-content qualifier-content']") %>%
    rvest::html_text() %>%
      {ifelse(length(.) == 0, NA_character_, .)})

  ipas <- purrr::map_chr(lis, ~rvest::html_nodes(., ".IPA") %>%
    rvest::html_text() %>%
      {ifelse(length(.) == 0, NA_character_, .)})

  tibble::tibble(type = descs, ipa = ipas)

}





#' Retrieve IPA data from specific word from Wiktionary
#' @import magrittr
#' @export
#'
#'
ipa <- function(term) {

  stopifnot(length(term) == 1L)

  pageid <- term_page_id(term)$id

    get_wiki_html(page_id = pageid) %>%
    extract_ipa()
}

#' Replace empty (length of 0) result by NA
#' @import magrittr

html_text_na <- function(xml) {

  stopifnot(class(xml) %in% c("xml_nodeset",
                              "xml_node",
                              "xml_missing"))

    rvest::html_text(xml) %>%
      {ifelse(length(.) == 0, NA_character_, .)}
}



#' Extract descriptions of audio files
#' @import magrittr
extract_audio_descs <- function(audio_tables) {

  if (length(audio_tables) == 0) return(NA_character_)

  purrr::map_chr(
    audio_tables,
    function(x) {
      rvest::html_node(
        x,
        xpath = "td[@class='unicode audiolink']") %>%
        html_text_na() %>%
        stringr::str_replace("^Audio", "") %>%
        stringr::str_trim() %>%
        stringr::str_replace_all("(^\\()|(\\)$)", "")
    }
  )

}

#' Extract names of audio files
#'
extract_file_names <- function(audio_tables) {

  if (length(audio_tables) == 0) return(NA_character_)

  purrr::map_chr(
    audio_tables,
    ~rvest::html_node(
      .,
      xpath = "td[@class='audiofile']/div/audio/source") %>%
      rvest::html_attr("src") %>%
      basename()
  )
}

#' Make xml content pretty
print_html <- function(content) {

  opts <- list(
    TidyDocType = "html5",
    TidyMakeClean = TRUE,
    TidyHideComments = TRUE,
    TidyIndentContent = TRUE,
    TidyBodyOnly = TRUE,
    TidyWrapLen = 100
  )

  htmltidy::tidy_html(content, option = opts)

}
