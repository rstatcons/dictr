#' Extract IPA data from HTML page
#' @param term_html Html object from Wiktionary
#' @return A tibble (data frame) with description and IPA.
#' @import magrittr
#' @import futile.logger
#' @import dplyr
#'
extract_ipa <- function(term_html) {

  lis <- rvest::html_nodes(
    x = term_html,
    # https://stackoverflow.com/a/23862266/3190110
    xpath = "//ul[count(preceding-sibling::h2)=1 and
    preceding-sibling::h2/span/@id='English']
    //li[descendant::span/@class='IPA']")

  if (length(lis) == 0L) {
    flog.trace("No IPA records detected.")
    return(tibble(type = character(0),
                  ipa = character(0)))
  }

  flog.trace("HTML li elements with IPA data:",
             cat(print_html(lis)),
             capture = TRUE)

  descs <- purrr::map_chr(lis, ~rvest::html_nodes(
    .,
    xpath = "span[not(@class = 'IPA') and
    @class = 'ib-content qualifier-content']") %>%
    rvest::html_text() %>%
      {ifelse(length(.) == 0, NA_character_, .)})

  ipas <- purrr::map_chr(lis, ~rvest::html_nodes(., ".IPA") %>%
    rvest::html_text() %>%
      {ifelse(length(.) == 0, NA_character_, .)})

  tibble::tibble(type = descs, ipa = ipas) %>%
    filter(stringr::str_detect(ipa, "(^/.+/$)|(^\\[.+\\]]$)"))

}
